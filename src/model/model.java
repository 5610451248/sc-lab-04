Description	Resource	Path	Location	Type
JComboBox is a raw type. References to generic type JComboBox<E> should be parameterized	Frame.java	/sc-lab-04/src/Frame	line 26	Java Problem
JComboBox is a raw type. References to generic type JComboBox<E> should be parameterized	Frame.java	/sc-lab-04/src/Frame	line 52	Java Problem
The import java.awt.BorderLayout is never used	Frame.java	/sc-lab-04/src/Frame	line 3	Java Problem
The import java.awt.Color is never used	Frame.java	/sc-lab-04/src/Frame	line 4	Java Problem
The import java.awt.event.ActionEvent is never used	Frame.java	/sc-lab-04/src/Frame	line 5	Java Problem
The import java.awt.event.ActionEvent is never used	InvestmentFrame.java	/sample-gui/src/gui	line 3	Java Problem
The import javax.swing.BorderFactory is never used	Frame.java	/sc-lab-04/src/Frame	line 8	Java Problem
The import javax.swing.ComboBoxModel is never used	Frame.java	/sc-lab-04/src/Frame	line 9	Java Problem
The import javax.swing.JFrame is never used	Controller.java	/sample-gui/src/Controller	line 5	Java Problem
The import javax.swing.JOptionPane is never used	Frame.java	/sc-lab-04/src/Frame	line 13	Java Problem
The import javax.swing.JPanel is never used	Frame.java	/sc-lab-04/src/Frame	line 14	Java Problem
The import javax.swing.JTextField is never used	Frame.java	/sc-lab-04/src/Frame	line 18	Java Problem
The import org.omg.CORBA.Environment is never used	SoftwareTest.java	/sc-lab-01/src/test	line 8	Java Problem
The serializable class Frame does not declare a static final serialVersionUID field of type long	Frame.java	/sc-lab-04/src/Frame	line 20	Java Problem
The serializable class InvestmentFrame does not declare a static final serialVersionUID field of type long	InvestmentFrame.java	/sample-gui/src/gui	line 15	Java Problem
The serializable class SoftwareFrame does not declare a static final serialVersionUID field of type long	SoftwareFrame.java	/sc-lab-01/src/gui	line 10	Java Problem
The value of the field Frame.str is not used	Frame.java	/sc-lab-04/src/Frame	line 29	Java Problem
The value of the field InvestmentFrame.INITIAL_BALANCE is not used	InvestmentFrame.java	/sample-gui/src/gui	line 21	Java Problem
The value of the field InvestmentFrame.str is not used	InvestmentFrame.java	/sample-gui/src/gui	line 28	Java Problem
Type safety: The constructor JComboBox(Object[]) belongs to the raw type JComboBox. References to generic type JComboBox<E> should be parameterized	Frame.java	/sc-lab-04/src/Frame	line 52	Java Problem